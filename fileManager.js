const fs = require('fs')

class FileManager {
    constructor(filename) {
        this.filename = filename;
    }

    fileToJson() {
        const isi = fs.readFileSync(this.filename, 'utf-8')
        let convertToJson = JSON.parse(isi)
        return convertToJson
    }

    saveChange(index, varName, varValue) {
        let data = this.fileToJson();
        if (index <= 0 || index > data.length) {
            return false
        }
        index--;
        data[index][varName] = varValue;
        fs.writeFileSync(this.filename, JSON.stringify(data))
        return true
    }
}

module.exports = FileManager