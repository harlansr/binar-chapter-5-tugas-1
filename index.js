const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});
const FileManager = require("./fileManager")

const consoleColor = {
    text: {
        black: "\x1b[30m%s\x1b[0m",
        red: "\x1b[31m%s\x1b[0m",
        green: "\x1b[32m%s\x1b[0m",
        yellow: "\x1b[33m%s\x1b[0m",
        blue: "\x1b[34m%s\x1b[0m",
        magenta: "\x1b[35m%s\x1b[0m",
        cyan: "\x1b[36m%s\x1b[0m",
        white: "\x1b[37m%s\x1b[0m"
    }
}

const filename = "./data.txt"
const fileM = new FileManager(filename)
let data = fileM.fileToJson();
let countData = data.length;


showAllData()






function refreshData() {
    data = fileM.fileToJson();
    countData = data.length;
    console.clear();
}

function showAllData() {
    refreshData()
    console.log(`-------------------------------------------`)
    console.log(``)
    if (countData <= 0) {
        console.log(consoleColor.text.red, 'Data tidak ditemukan');
        exitApp();
    }

    console.log(consoleColor.text.green, `Menemukan ${countData} data`)

    let i = 0

    data.forEach(item => {
        i++
        console.log(`${i} - ${item.name}`)
    });
    console.log(consoleColor.text.cyan, `0 - Exit Aplikasi`)
    console.log(``)
    selectName()
}

function selectName() {
    readline.question(`Nomor berapa yang ingin diubah? : `, numChose => {
        numChoseConvert = Number(numChose)
        // readline.close();
        if (numChose == "0") {
            exitApp()
        } else if (typeof numChoseConvert == 'number' && numChoseConvert) {
            if (numChoseConvert > 0 && numChoseConvert <= countData) {
                console.log(consoleColor.text.yellow, `Mengubah nama "${data[numChoseConvert - 1].name}"`);
                changeName(numChoseConvert)
            } else {
                console.log(consoleColor.text.red, `Data dengan nomor ${numChoseConvert} tidak ditemukan`);
                selectName()
            }
        } else {
            console.log(consoleColor.text.red, numChose + " Bukan sebuah nomor");
            selectName()
        }
    });
}

function changeName(index) {
    readline.question(`Masukan nama baru: `, name => {
        if (name) {
            let doChange = fileM.saveChange(index, 'name', name)
            if (doChange) {
                console.log(consoleColor.text.green, `Mengganti nama menjadi ${name} berhasil!`);
            } else {
                console.log('Pastikan memasukan data dengan benar')
            }
        }
        showAllData();
    });
}

function exitApp() {
    readline.close();
    process.exit();
}




// https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color